// The contents of this file are in the public domain. See LICENSE_FOR_EXAMPLE_PROGRAMS.txt
/*

    This is an example showing how you might use dlib to create a reasonably 
    functional command line tool for object detection.  This example assumes 
    you are familiar with the contents of at least the following example 
    programs:
        - fhog_object_detector_ex.cpp
        - compress_stream_ex.cpp




    This program is a command line tool for learning to detect objects in images.  
    Therefore, to create an object detector it requires a set of annotated training 
    images.  To create this annotated data you will need to use the imglab tool 
    included with dlib.  It is located in the tools/imglab folder and can be compiled
    using the following commands.  
        cd tools/imglab
        mkdir build
        cd build
        cmake ..
        cmake --build . --config Release
    Note that you may need to install CMake (www.cmake.org) for this to work.  

    Next, let's assume you have a folder of images called /tmp/images.  These images 
    should contain examples of the objects you want to learn to detect.  You will 
    use the imglab tool to label these objects.  Do this by typing the following
        ./imglab -c mydataset.xml /tmp/images
    This will create a file called mydataset.xml which simply lists the images in 
    /tmp/images.  To annotate them run
        ./imglab mydataset.xml
    A window will appear showing all the images.  You can use the up and down arrow 
    keys to cycle though the images and the mouse to label objects.  In particular, 
    holding the shift key, left clicking, and dragging the mouse will allow you to 
    draw boxes around the objects you wish to detect.  So next, label all the objects 
    with boxes.  Note that it is important to label all the objects since any object 
    not labeled is implicitly assumed to be not an object we should detect.  If there
    are objects you are not sure about you should draw a box around them, then double
    click the box and press i.  This will cross out the box and mark it as "ignore".
    The training code in dlib will then simply ignore detections matching that box.
    

    Once you finish labeling objects go to the file menu, click save, and then close 
    the program. This will save the object boxes back to mydataset.xml.  You can verify 
    this by opening the tool again with
        ./imglab mydataset.xml
    and observing that the boxes are present.

    Returning to the present example program, we can compile it using cmake just as we 
    did with the imglab tool.  Once compiled, we can issue the command 
        ./train_object_detector -tv mydataset.xml
    which will train an object detection model based on our labeled data.  The model 
    will be saved to the file object_detector.svm.  Once this has finished we can use 
    the object detector to locate objects in new images with a command like
        ./train_object_detector some_image.png
    This command will display some_image.png in a window and any detected objects will
    be indicated by a red box.

    Finally, to make running this example easy dlib includes some training data in the
    examples/faces folder.  Therefore, you can test this program out by running the
    following sequence of commands:
      ./train_object_detector -tv examples/faces/training.xml -u1 --flip
      ./train_object_detector --test examples/faces/testing.xml -u1
      ./train_object_detector examples/faces/*.jpg -u1
    That will make a face detector that performs perfectly on the test images listed in
    testing.xml and then it will show you the detections on all the images.
*/

#include <object_detector/object_detector_debug_only.h>


#define DEBUG_MODE
#define REALKINECT


#ifdef REALKINECT
    std::string rgbTopic = "/camera/rgb/image_rect_color";
#else
    std::string rgbTopic("/camera/rgb/image_raw");
#endif

ObjectDetector::ObjectDetector(int argc, char** argv) : it_(nh_), image_sub_(it_, rgbTopic, 1),
    depth_sub_(it_, "/camera/depth/image_raw", 1), sync(MySyncPolicy(10), image_sub_, depth_sub_)
    {
        // Subscrive to input video feed and publish output video feed
        /*image_sub_ = it_.subscribe("/camera/rgb/image_raw", 2,
        &ObjectDetector::imageCb, this);
        depth_sub_ = it_.subscribe("/camera/depth/image_raw", 2, &ObjectDetector::depthCb, this);*/

        sync.registerCallback(boost::bind(&ObjectDetector::imagesCb, this, _1, _2));

        //dlib initializing

        parser.add_option("nose_detector", "Path to svm - previously trained nose detector by train_object_detector",1);
        parser.add_option("side_detector", "Path to svm - previously trained side detector by train_object_detector",1);
        parser.add_option("back_detector", "Path to svm - previously trained back detector by train_object_detector",1);
        parser.add_option("front_detector", "Path to svm - previously trained front detector by train_object_detector",1);

        parser.add_option("u", "Upsample each input image <arg> times. Each upsampling quadruples the number of pixels in the image (default: 0).", 1);

        parser.parse(argc, argv);
        parser.check_option_arg_range("u", 0, 8);

        // Get the upsample option from the user but use 0 if it wasn't given.
        upsample_amount = get_option(parser, "u", 0);
        pathToNoseSVM = get_option(parser, "nose_detector", "null");
        pathToSideSVM = get_option(parser, "side_detector", "null");
        pathToBackSVM = get_option(parser, "back_detector", "null");
        pathToFrontSVM = get_option(parser, "front_detector", "null");

        if(pathToNoseSVM == "null" || pathToSideSVM == "null" || pathToBackSVM == "null" || pathToFrontSVM == "null")
        {
        ROS_ERROR_STREAM("You need to give previously trained object detector as a .svm file (check train_object_detector for details)");
        throw std::runtime_error("You need to give previously trained object detector as a .svm file (check train_object_detector for details)");
        }

        // load a previqrously trained object detector and try it out on some data
        ifstream finNose(pathToNoseSVM, ios::binary);
        if (!finNose)
        {
            ROS_ERROR_STREAM("Can't find a trained nose detector file "+pathToNoseSVM+".svm.\n");
            throw std::runtime_error("Can't find a trained object detector file "+pathToNoseSVM+".svm.");
        }

        ifstream finSide(pathToSideSVM, ios::binary);
        if (!finSide)
        {
            ROS_ERROR_STREAM("Can't find a trained side detector file "+pathToSideSVM+".svm.\n");
            throw std::runtime_error("Can't find a trained object detector file "+pathToSideSVM+".svm.");
        }

        ifstream finBack(pathToBackSVM, ios::binary);
        if (!finBack)
        {
            ROS_ERROR_STREAM("Can't find a trained back detector file "+pathToBackSVM+".svm.\n");
            throw std::runtime_error("Can't find a trained object detector file "+pathToBackSVM+".svm.");
        }

        ifstream finFront(pathToFrontSVM, ios::binary);
        if (!finBack)
        {
            ROS_ERROR_STREAM("Can't find a trained front detector file "+pathToFrontSVM+".svm.\n");
            throw std::runtime_error("Can't find a trained object detector file "+pathToFrontSVM+".svm.");
        }
    
        deserialize(detector_nose, finNose);
        deserialize(detector_side, finSide);
        deserialize(detector_back, finBack);
        deserialize(detector_front, finFront);

        //dlib INITIALIZED

        #ifdef DEBUG_MODE
        cv::namedWindow(OPENCV_WINDOW);
        cv::namedWindow(COLOR_WINDOW);
        cv::namedWindow(DEBUG_WINDOW);
        #endif
    }

    ObjectDetector::~ObjectDetector()
    {
        #ifdef DEBUG_MODE
        cv::destroyWindow(OPENCV_WINDOW);
        cv::destroyWindow(COLOR_WINDOW);
        cv::destroyWindow(DEBUG_WINDOW);
        #endif
    }

    void ObjectDetector::detectPeople()
    {

        //
        //To get the 3D position use: point.y=(y - centerY) / depthFocalLength; point.x=(x - centerX) / depthFocalLength; point.z=depthvalue_at(y,x);
        //
        
        if(cv_depth_ptr == nullptr || cv_ptr == nullptr || robotPosition.x == std::numeric_limits<double>::min())
        return; 

        array2d<unsigned char> image;

        std::lock_guard<std::mutex> lock(mux);

        try
        {
        assign_image(image, cv_image<unsigned char>(cv_ptr->image));
        for (unsigned long i = 0; i < upsample_amount; ++i)
        {
            pyramid_up(image);
        }

        // Run the detector on images[i] 
        rects_nose = detector_nose(image);
        //cout << "Number of nose detections: " << rects_nose.size() << endl;
        for(int i = 0; i < rects_nose.size(); i++)
        {
            //get points positioned in scaled window
            double x = dlib::center(rects_nose[i]).x();
            double y = dlib::center(rects_nose[i]).y();

            //size of current, scaled window
            cv::Size size = cv_ptr->image.size();

            //convert coords to unscaled, initial window
            x = initialSize.width/2 + (x - initialSize.width/2)/2;
            y = initialSize.height/2 + (y - initialSize.height/2)/2;

            #ifdef DEBUG_MODE
            cv::rectangle(debug_cv_ptr->image, cv::Point(x-5, y-5), cv::Point(x+5,y+5),cv::Scalar(128,128,128), 2);
            cv::rectangle(cv_ptr_color->image, cv::Point(x-5, y-5), cv::Point(x+5,y+5),cv::Scalar(128,128,128), 2);
            #endif
        }

        rects_side = detector_side(image);
        //cout << "Number of side detections: "<< rects_side.size() << endl;
        for(int i = 0; i < rects_side.size(); i++)
        {
            //get points positioned in scaled window
            double x = dlib::center(rects_side[i]).x();
            double y = dlib::center(rects_side[i]).y();

            //size of current, scaled window
            cv::Size size = cv_ptr->image.size();

            //convert coords to unscaled, initial window
            x = initialSize.width/2 + (x - initialSize.width/2)/2;
            y = initialSize.height/2 + (y - initialSize.height/2)/2;

            #ifdef DEBUG_MODE
            cv::rectangle(debug_cv_ptr->image, cv::Point(x-5, y-5), cv::Point(x+5,y+5),cv::Scalar(128,128,128), 2);
            cv::rectangle(cv_ptr_color->image, cv::Point(x-5, y-5), cv::Point(x+5,y+5),cv::Scalar(128,128,128), 2);

            #endif
        }

        rects_back = detector_back(image);
        //cout << "Number of back detections: "<< rects_back.size() << endl;
        for(int i = 0; i < rects_back.size(); i++)
        {
            //get points positioned in scaled window
            double x = dlib::center(rects_back[i]).x();
            double y = dlib::center(rects_back[i]).y();

            //size of current, scaled window
            cv::Size size = cv_ptr->image.size();

            //convert coords to unscaled, initial window
            x = initialSize.width/2 + (x - initialSize.width/2)/2;
            y = initialSize.height/2 + (y - initialSize.height/2)/2;

            #ifdef DEBUG_MODE
            cv::rectangle(debug_cv_ptr->image, cv::Point(x-5, y-5), cv::Point(x+5,y+5),cv::Scalar(128,128,128), 2);
            cv::rectangle(cv_ptr_color->image, cv::Point(x-5, y-5), cv::Point(x+5,y+5),cv::Scalar(128,128,128), 2);
            #endif   
        }

        rects_front = detector_front(image);
        //cout << "Number of front detections: "<< rects_front.size() << endl;
        for(int i = 0; i < rects_front.size(); i++)
        {
            //get points positioned in scaled window
            double x = dlib::center(rects_front[i]).x();
            double y = dlib::center(rects_front[i]).y();

            //size of current, scaled window
            cv::Size size = cv_ptr->image.size();

            //convert coords to unscaled, initial window
            x = initialSize.width/2 + (x - initialSize.width/2)/2;
            y = initialSize.height/2 + (y - initialSize.height/2)/2;

            #ifdef DEBUG_MODE
            cv::rectangle(debug_cv_ptr->image, cv::Point(x-5, y-5), cv::Point(x+5,y+5),cv::Scalar(128,128,128), 2);
            cv::rectangle(cv_ptr_color->image, cv::Point(x-5, y-5), cv::Point(x+5,y+5),cv::Scalar(128,128,128), 2);
            #endif
        }

        //if(rects_front.size() != 0 || rects_back.size() != 0 || rects_side.size() != 0 || rects_nose.size() != 0)
            //cout << endl;

        #ifdef DEBUG_MODE
        for(int i = 0; i < rects_nose.size(); i++)
        {
            char t[10] = "NOSE";
            cv::rectangle(cv_ptr->image, cv::Point(static_cast<int>(dlib::center(rects_nose[i]).x()-50), static_cast<int>(dlib::center(rects_nose[i]).y()-35)), cv::Point(static_cast<int>(dlib::center(rects_nose[i]).x()+50), static_cast<int>(dlib::center(rects_nose[i]).y()+35)),cv::Scalar(128,128,128), 4, 8, 0);
            cv::putText(cv_ptr->image, t, cv::Point(static_cast<int>(dlib::center(rects_nose[i]).x()-40), static_cast<int>(dlib::center(rects_nose[i]).y()+10)), cv::FONT_HERSHEY_PLAIN, 2, cv::Scalar(128,128,128), 3);
        }

        for(int i = 0; i < rects_side.size(); i++)
        {
            char t[10] = "SIDE";
            cv::rectangle(cv_ptr->image, cv::Point(static_cast<int>(dlib::center(rects_side[i]).x()-50), static_cast<int>(dlib::center(rects_side[i]).y()-35)), cv::Point(static_cast<int>(dlib::center(rects_side[i]).x()+50), static_cast<int>(dlib::center(rects_side[i]).y()+35)),cv::Scalar(128,128,128), 4, 8, 0);
            cv::putText(cv_ptr->image, t, cv::Point(static_cast<int>(dlib::center(rects_side[i]).x()-35), static_cast<int>(dlib::center(rects_side[i]).y()+10)), cv::FONT_HERSHEY_PLAIN, 2, cv::Scalar(128,128,128), 3);
        }

        for(int i = 0; i < rects_back.size(); i++)
        {
            char t[10] = "BACK";
            cv::rectangle(cv_ptr->image, cv::Point(static_cast<int>(dlib::center(rects_back[i]).x()-50), static_cast<int>(dlib::center(rects_back[i]).y()-35)), cv::Point(static_cast<int>(dlib::center(rects_back[i]).x()+50), static_cast<int>(dlib::center(rects_back[i]).y()+35)),cv::Scalar(128,128,128), 4, 8, 0);
            cv::putText(cv_ptr->image, t, cv::Point(static_cast<int>(dlib::center(rects_back[i]).x()-40), static_cast<int>(dlib::center(rects_back[i]).y()+10)), cv::FONT_HERSHEY_PLAIN, 2, cv::Scalar(128,128,128), 3);
        }

        for(int i = 0; i < rects_front.size(); i++)
        {
            char t[10] = "FRONT";
            cv::rectangle(cv_ptr->image, cv::Point(static_cast<int>(dlib::center(rects_front[i]).x()-60), static_cast<int>(dlib::center(rects_front[i]).y()-35)), cv::Point(static_cast<int>(dlib::center(rects_front[i]).x()+60), static_cast<int>(dlib::center(rects_front[i]).y()+35)),cv::Scalar(128,128,128), 4, 8, 0);
            cv::putText(cv_ptr->image, t, cv::Point(static_cast<int>(dlib::center(rects_front[i]).x()-40), static_cast<int>(dlib::center(rects_front[i]).y()+10)), cv::FONT_HERSHEY_PLAIN, 2, cv::Scalar(128,128,128), 3);
        }

        if(debug_cv_ptr != nullptr)
        imshow(DEBUG_WINDOW, debug_cv_ptr->image);
        imshow(COLOR_WINDOW, cv_ptr_color->image);
        imshow(OPENCV_WINDOW, cv_ptr->image);
        cv::waitKey(3);
        #endif
        }
        catch (exception& e)
        {
            cout << "\nexception thrown!" << endl;
            cout << e.what() << endl;
            return;
        }
    }

    void ObjectDetector::imagesCb(const sensor_msgs::ImageConstPtr& rgb_image, const sensor_msgs::ImageConstPtr& depth_image)
    {
        try
        {
            //rgb image callback

            cv_ptr = cv_bridge::toCvCopy(rgb_image, sensor_msgs::image_encodings::BGR8);
            cv_ptr_color = cv_bridge::toCvCopy(rgb_image, sensor_msgs::image_encodings::BGR8);
            cvtColor(cv_ptr->image, cv_ptr->image, cv::COLOR_RGB2GRAY);
            initialSize = cv_ptr->image.size();
            resize(cv_ptr->image, cv_ptr->image, cv::Size(), zoom, zoom);
            cv::Size size = cv_ptr->image.size();
            cv::Rect roi = cv::Rect((size.width-initialSize.width)/2, (size.height-initialSize.height)/2, initialSize.width, initialSize.height);
            cv_ptr->image = cv::Mat(cv_ptr->image, roi);

            //depth image callback
            cv_depth_ptr = cv_bridge::toCvCopy(depth_image, sensor_msgs::image_encodings::TYPE_32FC1);
        #ifdef DEBUG_MODE
            debug_cv_ptr = cv_bridge::toCvCopy(depth_image, sensor_msgs::image_encodings::TYPE_32FC1);
            double min;
            double max;
            cv::minMaxIdx(debug_cv_ptr->image, &min, &max);
            float scale = 255/(max-min);
            debug_cv_ptr->image.convertTo(debug_cv_ptr->image, CV_8UC1, 255, 0);
            applyColorMap(debug_cv_ptr->image, debug_cv_ptr->image, cv::COLORMAP_AUTUMN);
        #endif
        }
        catch(cv_bridge::Exception& e)
        {
            ROS_ERROR("cv_bridge exception: %s", e.what());
            return;
        }
    }

// ----------------------------------------------------------------------------------------


int main(int argc, char** argv)
{  
    ros::init(argc, argv, "nose_detector");
    std::shared_ptr<ObjectDetector> detector;

    try{
        detector = make_shared<ObjectDetector>(argc, argv);
    }
    catch (exception& e)
    {
        cout << "\nexception thrown!" << endl;
        cout << e.what() << endl;
        ROS_ERROR_STREAM(e.what());
        return 1;
    }

    while(ros::ok())
    {
        try{
            ros::spinOnce();
            ros::spinOnce();
            detector->detectPeople();
        }
        catch (exception& e)
        {
            cout << "\nexception thrown!" << endl;
            cout << e.what() << endl;
            ROS_ERROR_STREAM(e.what());
            return 1;
        }
    }

    return 0;
}

// ----------------------------------------------------------------------------------------

